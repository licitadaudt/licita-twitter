import tweepy
import consts


class Tweet():

    def __init__(self, json_api):
        self.id = json_api["id"]
        self.user_name = json_api["user"]["screen_name"]
        self.created_at = json_api["created_at"]
        self.url = json_api["entities"]['urls'][0]['url']

class Twitter(): 
    
    def __init__(self):
        self.consumer_key = consts.CONSUMER_KEY
        self.consumer_secret = consts.CONSUMER_SECRET
        self.access_token = consts.ACCESS_TOKEN 
        self.access_token_secret = consts.ACCESS_TOKEN_SECRET 
        self.do_authentication()
    
    def do_authentication(self):
        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        auth.set_access_token(self.access_token, self.access_token_secret)
        self.api = tweepy.API(auth)

    def tweet(self, text, image_path = None):
        if image_path is not None:
            response = self.api.update_with_media(image_path, text)
            t = Tweet(response._json)
            return t
        else:
            response = self.api.update_status(text)
            t = Tweet(response._json)
            return t
            

