import pymysql
import datetime
import sys
import consts
import time
from twitter import Twitter

access_token = "763668589-Y7v2D1YhL1ETHlIda7zZ0JEnbdVnAC99KzDlEcLk"
secret = "zdm5vivpX0k4Weov20LdOb0jFodFUVmCzhJv4hKVW2MQF"

class MysqlDB:
    
    instance = None

    @staticmethod
    def get_instance():
        if MysqlDB.instance is None:
            MysqlDB.instance = MysqlDB()
        return MysqlDB.instance

    def __init__(self):
        self.user = consts.DB_USER
        self.host = consts.DB_HOST
        self.password = consts.DB_PASSWORD
        self.database = consts.DB_NAME
        self.connect(self.user, self.host, self.password, self.database)

    def connect(self, user, host, password, database):
        self.connection = pymysql.connect(host=host, user=user, passwd=password, db=database)
        return self.connection

    def execute_query(self, query, args=None):
        cursor = self.connection.cursor(pymysql.cursors.DictCursor)
        cursor.execute(query, args)
        return cursor


class LicitacoesService:

    def __init__(self):
        self.db = MysqlDB.get_instance()

    def fetch_next_licitacao_to_tweet(self):        #SEARCH IN DATABASE FOR THE NEXT ITEM TO PUBLISH
        return self.fetch_one("SELECT * FROM licitacoes WHERE tweet=0 AND remove = 0 AND dt_abertura  > now() ORDER BY id LIMIT 1")
        #ADICIONAR AQUI UM FINAL CASO NAO ENCONTRE NADA PRA PUBLICAR
    
    def fetch(self, query, args):
        pass
    
    def fetch_one(self, query, args = None):
        cursor = self.db.execute_query(query, args)
        if cursor.rowcount >= 1:
            return Licitacao.from_dict(cursor.fetchone())
    
    def update_as_tweeted(self, licitacao_id):
        query = "UPDATE licitacoes set tweet=1 WHERE id = %s"
        self.db.execute_query(query,[licitacao_id])
    
class Licitacao:

    def __init__(self):
        self.id = None
        self.ordem = None
        self.link = None
        self.objeto = None
        self.localidade = None
        self.licitacao = None
        self.dt_abertura = None
        self.UASG = None
        self.uor = None
        self.data_insert = None
        self.tweet = None
        self.remove = None

    def get_objeto_for_twitter(self):
        max_len = 182
        if len(self.objeto) > max_len + 3:
            return self.objeto[0:max_len] + "..."
        return self.objeto

    def get_localidade_for_twitter(self):
        max_len = 225
        if len(self.localidade) > 1:
            return self.localidade[-2:0]

        return self.localidade
            
    def to_twitter_str(self):
        text = """{} - {}

Veja mais: {}
           """.format(self.get_localidade_for_twitter(), self.get_objeto_for_twitter(), "http://online.daudt.eng.br/licitacoes")
        return text

    @staticmethod
    def from_dict(dict):
        licitacao = Licitacao()
        for key in dict:
            setattr(licitacao,key, dict[key])
        return licitacao

class Main:
    
    DEFAULT_DATE_FORMAT = "%Y-%m-%d %H:%M%S"

    def __init__(self):
        self.licitacao_service = LicitacoesService()
        self.twitter = Twitter()

    def get_last_post_time(self):
        try:
            _file = open("last_post_time.txt", "r")
            for line in _file:
                return datetime.datetime.strptime(line.strip(), Main.DEFAULT_DATE_FORMAT)
        except FileNotFoundError as err:
            return None


    def set_last_post_time(self, post_datetime):
        _file = open("last_post_time.txt", "w+")
        _file.write((post_datetime.strftime(Main.DEFAULT_DATE_FORMAT)))
        _file.close()

    def can_post(self, interval_in_hours):
        last_post_datetime = self.get_last_post_time()
        if last_post_datetime is None:
            return True
        now_datetime = datetime.datetime.now()
        diff_hours = (now_datetime - last_post_datetime).seconds/60/60
        return diff_hours > interval_in_hours

    def tweet_next_licitacao(self, interval_in_hours):
        if self.can_post(interval_in_hours):
            li = self.licitacao_service.fetch_next_licitacao_to_tweet()
            if self.twitter.tweet(li.to_twitter_str(), "licitacao.jpg"):
                self.licitacao_service.update_as_tweeted(li.id)
                self.set_last_post_time(datetime.datetime.now())
                print("TWEETED")
        else:
            print("WILL NOT SEND")

    def run(self, interval_in_hours):
        #while True:
            print("SENDING TWEET")
            self.tweet_next_licitacao(interval_in_hours)
            print("MY WORK HERE IS DONE. TAKE CARE.")
            time.sleep(3)

if __name__ == "__main__":
    if len(sys.argv) >= 2:
        interval_in_hours = int(sys.argv[1])
    else:
        interval_in_hours = 1
    m = Main()
    m.run(interval_in_hours)
